#pragma once
#include "vkdef.h"
#include "App.h"
#include "Window.h"

#ifndef VK_NAMESPACE
#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }
#endif 

VK_NAMESPACE_BEG
class AppFileManager : public App{
private:
	class Wnd : public Window {
	private:
		friend class AppFileManager;
	public:
		Wnd();
	}wnd;
public:
	Pair<EVENTS, void*> OnKeyDown(unsigned char key, unsigned  char sub_key)override;
	void Update(const Time &time)override;
	void OnDraw()override;
	AppFileManager();
	~AppFileManager();
};
VK_NAMESPACE_END