#pragma once
#include "vkdef.h"
#include "Vector.h"
#ifndef VK_NAMESPACE
#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }
#endif 

VK_NAMESPACE_BEG
class Element;
class ElementManager {
protected:
	Vector<Element> mng;
public:
	void Push(Element &el) {
		mng.PushBack(el);
	}
	void Pop() {
		mng.PopBack();
	}
	Element& Get(int i) {
		return mng[i];
	}
	Element& operator[](int i) {
		return mng[i];
	}
	const Element& Get(int i)const {
		return mng[i];
	}
	const Element& operator[](int i)const {
		return mng[i];
	}
};
VK_NAMESPACE_END