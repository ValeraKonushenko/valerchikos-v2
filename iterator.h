#pragma once
#include "Resourses.h"
#pragma pack(push, VK_PACK)
#ifndef VK_NAMESPACE

#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }

#endif

VK_NAMESPACE_BEG
template<class _Ty>
class BaseIterator {
public:
	virtual operator _Ty() = 0;
};

VK_NAMESPACE_END
#pragma pack(pop)