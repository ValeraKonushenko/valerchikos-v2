#pragma once
#include "App.h"
#include "Window.h"
#include "Pair.h"
#include "TextBox.h"

#ifndef VK_NAMESPACE
#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }
#endif 

VK_NAMESPACE_BEG
class AppOSHelper : public App{
private:
	class Wnd : public Window {
	private:
		friend class AppOSHelper;
		void Draw() override;
	public:
		TextBox header;
		TextBox txt;
		short cur_el_focus;//Current focus on some element.
		Wnd();
	};
	Wnd wnd;
public:
	Pair<EVENTS, void*> OnKeyDown(unsigned char key, unsigned  char sub_key);
	void Update(const Time &time);
	void OnDraw();
	AppOSHelper();
	~AppOSHelper();
};
VK_NAMESPACE_END