#include "vkdef.h"
#include "AppOSHelper.h"
#include "AppEditWnd.h"
#include <ctime>

VK_NAMESPACE_BEG
AppOSHelper::AppOSHelper() {
	mwnd = &wnd;
}
AppOSHelper::~AppOSHelper(){

}

Pair<EVENTS, void*> AppOSHelper::OnKeyDown(unsigned char key, unsigned char sub_key){
	if (key == k_IsExpandKeyboar) {
		if (sub_key == k_Down_e) {
			if (wnd.txt.ScrollToDown()) {
				_UPDATE_PART_OF_THE_SCREEN__CLEAR
				wnd.txt.ClearArea();
				_UPDATE_PART_OF_THE_SCREEN__DISPLAY
				wnd.txt.Show();
			}
			return { VOS_KEY_WAS_USED, nullptr };
		}
		if (sub_key == k_Up_e) {
			if (wnd.txt.ScrollToUp()) {
				_UPDATE_PART_OF_THE_SCREEN__CLEAR
				wnd.txt.ClearArea();
				_UPDATE_PART_OF_THE_SCREEN__DISPLAY
				wnd.txt.Show();
			}
		}
		if (sub_key == k_F11_e) {
			if (this->wnd.GetShowMode() == InWindow)
				this->wnd.MakeShowModeAs_FULL_SCREEN();
			else
				this->wnd.MakeShowModeAs_IN_WINDOW(10, 10, 50, 20);

			return { VOS_SCREEN_UPDATE, nullptr };
		}
		WINDOW_EDITOR___RESERV_K_HOME(wnd)
			
	}
	return Pair<EVENTS, void*>(0,0);
}
void AppOSHelper::Update(const Time & time){}
void AppOSHelper::OnDraw(){
	wnd.Show();
}


void AppOSHelper::Wnd::Draw(){
	header.SetPosition({ this->w_x , this->w_y }, { this->wm_x , this->w_y + 1 });
	header.SetColor(bg, Color::Blue);
	header.SetData("MAIN RULES");
	header.SetAlign(Align::Center);
	header.Show();
	
	txt.SetPosition({ this->w_x , this->w_y + 1 }, { this->wm_x , this->wm_y });
	txt.SetColor(bg, Color::BrightWhite);
	txt.SetAlign(ToLeft);
	txt.SetData(" Cascading Style Sheets   \n(CSS) is a st\tyle sheet language used for describing the presentation of a document written in a markup language like HTML.[1] CSS is a cornerstone technology of the World Wide Web, alongside HTML and JavaScript.CSS is designed to enable the separation of presentation and content, including layout, colors, and fonts.[3] This separation can improve content accessibility, provide more flexibility and control in the specification of presentation characteristics, enable multiple web pages to share formatting by specifying the relevant CSS in a separate.css file, and reduce complexity and repetition in the structural content.Separation of formatting and cont. In addition to HTML, other markup languages support the use of CSS including XHTML, plain XML, SVG, and XUL");
	txt.Show();
}


AppOSHelper::Wnd::Wnd():
	Window("OS Helper", rand() % 10 + 3, rand() % 10 + 3, 60, 15, Color::Yellow, Color::BrightWhite){
}
VK_NAMESPACE_END