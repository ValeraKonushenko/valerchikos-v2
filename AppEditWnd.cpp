#include "vkdef.h"
#include "AppEditWnd.h"
#include <cstring>
VK_NAMESPACE_BEG
AppEditWnd::Wnd::Wnd(Window *out_wnd, short x, short y) :Window("Window editor", x,y,22,7,
	Color::Aqua, Color::BrightWhite) {
	short i = 0;
	edit_x.SetPosition(w_x, w_y + i++);
	edit_y.SetPosition(w_x, w_y + i++);
	edit_w.SetPosition(w_x, w_y + i++);
	edit_h.SetPosition(w_x, w_y + i++);
	is_fullscreen.SetPosition(w_x, w_y + i++);

	edit_x.SetColorForField(Color::Blue, Color::BrightWhite);
	edit_y.SetColorForField(Color::Blue, Color::BrightWhite);
	edit_w.SetColorForField(Color::Blue, Color::BrightWhite);
	edit_h.SetColorForField(Color::Blue, Color::BrightWhite);
	is_fullscreen.SetColorForField(Color::Blue, Color::BrightWhite);

	edit_x.SetColor(bg, fg);
	edit_y.SetColor(bg, fg);
	edit_w.SetColor(bg, fg);
	edit_h.SetColor(bg, fg);
	is_fullscreen.SetColor(bg, fg);

	edit_x.SetMaxInputLen(7);
	edit_y.SetMaxInputLen(7);
	edit_w.SetMaxInputLen(7);
	edit_h.SetMaxInputLen(7);

	edit_x.SetTitle("X:          ");
	edit_y.SetTitle("Y:          ");
	edit_w.SetTitle("Width:      ");
	edit_h.SetTitle("Height:     ");
	is_fullscreen.SetTitle("Fullscreen: ");

	edit_x.SetLock(LOCK__DIGIT);
	edit_y.SetLock(LOCK__DIGIT);
	edit_w.SetLock(LOCK__DIGIT);
	edit_h.SetLock(LOCK__DIGIT);
	edit_x.SetActive(1); 
	char buff[16]{0};

	itoa(out_wnd->GetPos().x, buff, 10);
	edit_x.SetData(buff);

	itoa(out_wnd->GetPos().y, buff, 10);
	edit_y.SetData(buff);

	itoa(out_wnd->GetW(), buff, 10);
	edit_w.SetData(buff);

	itoa(out_wnd->GetH(), buff, 10);
	edit_h.SetData(buff);
	
	if (out_wnd->IsFullScreen() == FullScreen)
		is_fullscreen.SetData(1);

}
void AppEditWnd::UpdateData(){
	out_wnd->SetPosition(static_cast<short>(atoi(wnd.edit_x.GetData())),
		static_cast<short>(atoi(wnd.edit_y.GetData())));
	out_wnd->SetSize(static_cast<uint>(atoi(wnd.edit_w.GetData())),
		static_cast<uint>(atoi(wnd.edit_h.GetData())));
	if (wnd.is_fullscreen.GetData() == true)
		out_wnd->MakeShowModeAs_FULL_SCREEN();
	else
		out_wnd->MakeShowModeAs_IN_WINDOW(out_wnd->GetPos().x, out_wnd->GetPos().y,
			out_wnd->GetW(), out_wnd->GetH());
}
AppEditWnd::AppEditWnd(Window *out_wnd, short x , short y):
	wnd(out_wnd,x,y){
	if (!out_wnd)throw Exception("Pointer to data us NULL");
	this->out_wnd = out_wnd;
	mwnd = &wnd;
	cur_field = 0;
	active_p = 0;
}
void AppEditWnd::OnDraw(){
	wnd.Show();
	wnd.edit_x.Show();
	wnd.edit_y.Show();
	wnd.edit_w.Show();
	wnd.edit_h.Show();
	wnd.is_fullscreen.Show();
}

Pair<EVENTS, void*> AppEditWnd::OnKeyDown(unsigned char key, unsigned char sub_key) {
	bool is = false;
	static short last_choice = 0;
	if (key == k_Space) {
		if (cur_field == 4) {
			if (wnd.is_fullscreen.GetData())
				wnd.is_fullscreen.SetData(0);
			else
				wnd.is_fullscreen.SetData(1);
			is = true;
		}
	}
	if (key == k_Enter) {
		cur_field++;
		is = true;
		if (cur_field >= 5){
			UpdateData();
			last_choice = 0;
			return { VOS_SCREEN_UPDATE | VOS_CLOSE_LAST_APP, nullptr };
		}
	}
	if (key == k_IsExpandKeyboar) {
		if (sub_key == k_Up_e && cur_field > 0) {
			cur_field--;
			is = true;
		}
		else if (sub_key == k_Down_e && cur_field < 4) {
			cur_field++;
			is = true;
		}
	}
	if (is) {
		if (last_choice == 0) {
			wnd.edit_x.SetActive(0);
			wnd.edit_x.Show();
		}
		else if (last_choice == 1) {
			wnd.edit_y.SetActive(0);
			wnd.edit_y.Show();
		}
		else if (last_choice == 2) {
			wnd.edit_w.SetActive(0);
			wnd.edit_w.Show();
		}
		else if (last_choice == 3) {
			wnd.edit_h.SetActive(0);
			wnd.edit_h.Show();
		}
		else if (last_choice == 4) {
			wnd.is_fullscreen.SetActive(0);
			wnd.is_fullscreen.Show();
		}

		if (cur_field == 0) {
			wnd.edit_x.SetActive(1);
			wnd.edit_x.Show();
		}
		else if (cur_field == 1) {
			wnd.edit_y.SetActive(1);
			wnd.edit_y.Show();
		}
		else if (cur_field == 2) {
			wnd.edit_w.SetActive(1);
			wnd.edit_w.Show();
		}
		else if (cur_field == 3) {
			wnd.edit_h.SetActive(1);
			wnd.edit_h.Show();
		}
		else if (cur_field == 4) {
			wnd.is_fullscreen.SetActive(1);
			wnd.is_fullscreen.Show();
		}
		last_choice = cur_field;
		is = false;
	}



	if (cur_field == 0) {
		if (wnd.edit_x.InputData(key, sub_key))
			wnd.edit_x.Show();
	}
	else if (cur_field == 1) {
		if (wnd.edit_y.InputData(key, sub_key))
			wnd.edit_y.Show();
	}
	else if (cur_field == 2) {
		if (wnd.edit_w.InputData(key, sub_key))
			wnd.edit_w.Show();
	}
	else if (cur_field == 3) {
		if (wnd.edit_h.InputData(key, sub_key))
			wnd.edit_h.Show();
	}



	return Pair<EVENTS, void*>();
}

void AppEditWnd::Update(const Time & time){
	if (cur_field == 0) {
		Console.SetCursorPositon(wnd.edit_x.GetBeg().x + wnd.edit_x.GetCaret()
			+ strlen(wnd.edit_x.GetTitle()), wnd.edit_x.GetBeg().y);
	}
	else if (cur_field == 1) {
		Console.SetCursorPositon(wnd.edit_y.GetBeg().x + wnd.edit_y.GetCaret()
			+ strlen(wnd.edit_y.GetTitle()), wnd.edit_y.GetBeg().y);
	}
	else if (cur_field == 2) {
		Console.SetCursorPositon(wnd.edit_w.GetBeg().x + wnd.edit_w.GetCaret()
			+ strlen(wnd.edit_w.GetTitle()), wnd.edit_w.GetBeg().y);
	}
	else if (cur_field == 3) {
		Console.SetCursorPositon(wnd.edit_h.GetBeg().x + wnd.edit_h.GetCaret() 
			+ strlen(wnd.edit_h.GetTitle()), wnd.edit_h.GetBeg().y);
	}
}

VK_NAMESPACE_END