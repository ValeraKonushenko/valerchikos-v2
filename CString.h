#pragma once

#ifndef VK_NAMESPACE
#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }
#endif

VK_NAMESPACE_BEG
/*~~~~~~~~~~~~~~CHAR~~~~~~~~~~~~~~*/
unsigned int	StrLen(const char *str);
short			StrCmp(const char *str1, const char *str2);
void			StrCpy(char *str1, const char *str2);



/*~~~~~~~~~~~~~~SHORT~~~~~~~~~~~~~~*/
unsigned int	StrLen(const short *str);
short			StrCmp(const short *str1, const short *str2);
void			StrCpy(short *str1, const short *str2);
VK_NAMESPACE_END