#pragma once
#include "App.h"
#include "Vector.h"
#include "vkdef.h"
#include "WNDGeneral.h"

#ifndef VK_NAMESPACE
#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }
#endif 
VK_NAMESPACE_BEG

class AppManager{
private:
	Vector<App*> apps;
public:
	void		PushApp		(App* p);
	void		PopApp		();
	App*		GetApp		(int i);
	App*		GetLastApp	();
	int			GetAmoApp	();
	void		UpdateAllWindow();
	AppManager();
	~AppManager() = default;
	DISABLE_COPY_CLASS(AppManager)
};
VK_NAMESPACE_END