#pragma once
/*~~~~~~~~precompile header~~~~~~~~~~~~*/
#pragma warning (disable: 4458)
#include "Resourses.h"
#include "Exception.h"
#include <chrono>
#define VOS_FALSE					0b00000000
#define VOS_SCREEN_UPDATE			0b00000001
#define VOS_KEY_NOT_USED			0b00000010
#define VOS_KEY_WAS_USED			0b00000100
#define VOS_SCREEN_UPDATE_IS_APP	0b00001001
#define VOS_START_APP				0b00001000
#define VOS_CLOSE_LAST_APP			0b00010000

#define STR_SZ						1024

#define _UPDATE_PART_OF_THE_SCREEN__CLEAR
#define _UPDATE_PART_OF_THE_SCREEN__DISPLAY

#define DISABLE_COPY_CLASS(T)		T(const T&) = delete;\
									T(const T&&) = delete;\
									T& operator=(const T&) = delete;\
									T& operator=(const T&&) = delete;
using Time = std::chrono::time_point<std::chrono::steady_clock>;
#define CPoint vk::Vector2<short>
typedef unsigned char BYTE;
typedef unsigned char EVENTS;
enum Keys {
	k_none				= 0,
	k_Backspace			= 8,
	k_Tab				= '\t',
	k_Enter				= 13,
	k_Space				= 32,
	k_Accent			= '`',
	k_Right_e			= 77,
	k_Left_e			= 75,
	k_Up_e				= 72,
	k_Down_e			= 80,
	k_F12_e				= 134,
	k_F11_e				= 133,
	k_Home_e			= 71,
	k_ESC				= 27,
	k_IsExpandKeyboar	= 224
};
enum Direction {
	dir_Down,
	dir_Up,
	dir_Left,
	dir_Right
};
enum Align {
	ToBottom,
	ToTop,
	ToLeft,
	ToRight,
	Center,
	VerticalCenter
};
enum ShowMode {
	FullScreen,
	InWindow
};

#define	LOCK__NOT		0b00000000	//full access
#define	LOCK__ALPHA		0b00000001	//you can input only alphabet
#define	LOCK__DIGIT	 	0b00000010	//you can input only digits
#define	LOCK__EXTRA 	0b00000010	//you can input only extra symbols: ";:'!@#$%^&..."
#pragma warning(disable: 4244)