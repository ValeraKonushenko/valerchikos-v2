#include "vkdef.h"
#include "AppManager.h"


VK_NAMESPACE_BEG
			AppManager::AppManager	(){
	apps.Reserve(10);
}
void		AppManager::PushApp		(App * p){
	apps.PushBack(p);
}
void		AppManager::PopApp		(){
	apps.PopBack();
}
App *		AppManager::GetApp		(int i){
	return apps.GetAt(i);
}
App * AppManager::GetLastApp(){
	if(apps.Size() != 0)
		return apps.GetAt(apps.Size() - 1);
	return nullptr;
}
int			AppManager::GetAmoApp	(){
	return apps.Size();
}

void AppManager::UpdateAllWindow(){
	static int len = 0, amo_FS = 0, pos_first_FS = 0;//FS - fullscreen
	amo_FS = pos_first_FS = 0;
	len = this->apps.Size();
	int i = 0;
	for (i = 0; i < len; i++)
		if (apps[i]->GetWnd()->GetShowMode() == FullScreen)
		{
			pos_first_FS = i;
			amo_FS++;
		}

	for (i = 0; i < len; i++)
		apps[i]->OnDraw();
}



VK_NAMESPACE_END