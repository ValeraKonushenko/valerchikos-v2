#pragma once
#include "Resourses.h"
#include "Exception.h"
#include "CString.h"
#ifndef VK_NAMESPACE
#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }
#endif

VK_NAMESPACE_BEG
class String{
public:
	using type =	char;
					String			(const type *s = 0);
					String			(int reserve);
					String			(String &s);
					~String			();
	String&			FillZero		(uint amo);
	uint			Length			() const;
	uint			Capacity		() const;
	String &		Clear			();
	//Overlaying new data on the old data
	String &		Copy			(const type *s, uint pos = 0, uint amo = 0);
	String &		Copy			(const String & s, uint pos = 0, uint amo = 0);
	String &		Cat				(const type *s, uint amo = 0);
	String &		Cat				(const String & s, uint amo = 0);
	String &		Cat				(const type ch);
	const type *	c_str			()const;
	bool			IsEmpty			()const;
	String&			Erase			(uint pos, uint amo);
	String			SubStr			(uint pos, uint amo)const;
	int				Find			(const type *s)const;
	int				FindFirstOf		(char t)const;
	int				FindLastOf		(char t)const;
	int				FindFirstNotOf	(char t)const;
	int				FindLastNotOf	(char t)const;
	String &		Insert			(uint pos, const type *s);
	String &		Insert			(uint pos, const type s);
	short			Compare			(String &s)const;
	short			Compare			(const type *s)const;
	//Coping old to new arr(all data are save), and expanding to new capacity/size
	String &		Resize			(uint sz);
	//Deleting all data, and expanding to new capacity/size
	String &		Reverse			();
	String &		Reserve			(uint new_cap);
	type &			operator []		(uint i);
	String &		operator =		(const type *s);
	String &		operator =		(String &s);
	String &		operator +=		(const type *s);
	String &		operator +=		(String &s);
	String &		operator +=		(type s);
	String			operator +		(const type *s);
	String			operator +		(String &s);
	bool			operator >		(const type *s)const;
	bool			operator >		(String &s)const;
	bool			operator <		(const type *s)const;
	bool			operator <		(String &s)const;
	bool			operator >=		(const type *s)const;
	bool			operator >=		(String &s)const;
	bool			operator <=		(const type *s)const;
	bool			operator <=		(String &s)const;
	bool			operator ==		(const type *s)const;
	bool			operator ==		(String &sconst)const;
	bool			operator !=		(const type *s)const;
	bool			operator !=		(String &s)const;
	bool			operator !		()const;

					operator const type *()const;
	friend String operator + (const type * str1, String & str2);
protected:
	type			*str;
	uint	size;
	uint	capacity;
};
String ConvertIntToString(int c);
VK_NAMESPACE_END