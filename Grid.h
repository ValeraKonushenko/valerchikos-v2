#pragma once
#include "vkdef.h"
#include "Vector2.h"
#include "Vector.h"
#ifndef VK_NAMESPACE
#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }
#endif 

VK_NAMESPACE_BEG

class GridTitle {
protected:
	friend class Grid;

	//~~~~~~~~~~~~DATA~~~~~~~~~~~~//
	CPoint	beg, end;
	char			*data;


	//~~~~~~~~~~~~PRVT FUNCTION~~~~~~~~~~~~//
	void SetPosition(CPoint	beg, CPoint	end);
public:
	GridTitle();
	GridTitle&		SetData(const char *data);
	const char *	GetData();
	void			ShowData(bool is_active = false, bool is_text = false, bool is_align = false)const;
	CPoint	GetPBeg();
	CPoint	GetPEnd();
	short	GetW();
	short	GetH();
	~GridTitle();
};



class Grid{
private:
	Vector<GridTitle>		titles;
	CPoint			beg, end;
	CPoint			sz_workspace;
	CPoint			sz_title;
	int row, col;
public:
	Grid					(CPoint L_U_corner, CPoint R_D_corner, short row, short col);
	GridTitle& GetTitle		(short x, short y);
	GridTitle& GetTitle		(short i);
	int GetAmoTitle			()const;
	int GetAmoRow			()const;
	int GetAmoCol			()const;
	void RebuildAccordintToNewRect(CPoint beg, CPoint end);
	~Grid() = default;
};
VK_NAMESPACE_END