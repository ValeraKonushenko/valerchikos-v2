#pragma once
#include "Element.h"
#include "Console.h"
#include "String.h"
#include "vkdef.h"

#ifndef VK_NAMESPACE
#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }
#endif 

VK_NAMESPACE_BEG
class CheckBox : public Element {
private:
	Color field_bg, field_fg;
	String title;
	bool is_active, data;
public:
	CheckBox() {
		data = false;
		is_active = false;
		title = "";
		field_bg = Color::Blue;
		field_fg = Color::BrightWhite;
	}
	void SetActive(bool act) {
		is_active = act;
	}
	bool GetIsActive()const {
		return is_active;
	}
	void SetData(bool data) {
		this->data = data;
	}
	bool GetData()const {
		return data;
	}
	void SetPosition(short x, short y) {
		beg = { x, y };
		end.x = beg.x + 1 + title.Length();
		end.y = beg.y + 1;
	}
	void SetColorForField(Color bg, Color fg) {
		field_bg = bg;
		field_fg = fg;
	}
	void SetTitle(const char *str) {
		title = str;
		end.x = beg.x + 1 + title.Length();
	}
	const char * GetTitle()const {
		return title.c_str();
	}
	void Show() const override {
		//TITLE
		Console.SetColor(bg, fg);
		Console.SetCursorPositon(beg.x, beg.y).Write(title);

		//FIELD
		WORD old_color = Console.GetColor();
		if (is_active)
			Console.SetColor(Color::Red, field_fg);
		else			Console.SetColor(field_bg, field_fg);

		if(data)
			Console.Write("#");
		else
			Console.Write(" ");
		Console.SetCursorPositon(beg.x + title.Length(), beg.y);

		if (is_active)	Console.SetColor(static_cast<Color>(old_color));
	}
};
VK_NAMESPACE_END