#pragma once
#include "App.h"
#include "Window.h"
#include "Input.h"
#include "CheckBox.h"
#ifndef VK_NAMESPACE
#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }
#endif 

VK_NAMESPACE_BEG
class AppEditWnd : public App{
private:
	class Wnd : public Window {
	public:
		Input edit_x, edit_y;
		Input edit_w, edit_h;
		CheckBox is_fullscreen;
		Wnd(Window *out_wnd, short x, short y);
	}wnd;
	Window *out_wnd;
	short cur_field;
	short active_p;
	void UpdateData();
public:
	AppEditWnd(Window *out_wnd, short x = 10, short y = 4);
	void OnDraw()override;
	Pair<EVENTS, void*> OnKeyDown(unsigned char key, unsigned  char sub_key)override;
	void Update(const Time &time)override;
};
VK_NAMESPACE_END