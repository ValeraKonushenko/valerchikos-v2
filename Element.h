#pragma once
#include "vkdef.h"
#include "Console.h"
#include "WNDGeneral.h"

#ifndef VK_NAMESPACE
#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }
#endif 

VK_NAMESPACE_BEG
class Element {
protected:
	CPoint beg, end;//beg - left-up corner;   end - down-right corner; 
	Color bg, fg;
public:
	Element() {
		beg = end = 0;
		bg = Color::Black;
		fg = Color::BrightWhite;
	}
	void			SetPosition(CPoint beg, CPoint end);
	void			SetColor(Color bg, Color fg);
	void			SetBgColor(Color color);
	CPoint 			GetBeg()	const;
	void			SetFgColor(Color color);
	CPoint 			GetEnd()	const;
	short 			GetH()		const;
	short 			GetW()		const;
	void			ClearArea() const;
	virtual void	Show()		const;
	virtual			~Element(){}
};
VK_NAMESPACE_END