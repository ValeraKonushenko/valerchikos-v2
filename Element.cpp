#include "vkdef.h"
#include "Element.h"
VK_NAMESPACE_BEG
void			Element::SetPosition(CPoint beg, CPoint end) {
	this->beg = beg;
	this->end = end;
}
void			Element::SetColor(Color bg, Color fg) {
	this->bg = bg;
	this->fg = fg;
}
void			Element::SetBgColor(Color color) {
	bg = color;
}
void			Element::SetFgColor(Color color) {
	fg = color;
}
CPoint			Element::GetBeg()const {
	return CPoint(beg);
}
CPoint			Element::GetEnd()const {
	return CPoint(end);
}
short			Element::GetH() const {
	return end.y - beg.y;
}
short			Element::GetW() const {
	return end.x - beg.x;
}
void			Element::ClearArea() const{
	Console.SetColor(bg, fg);
	short w = end.x - beg.x;
	for (int i = beg.y; i < end.y; i++)
	{
		Console.SetCursorPositon(beg.x, i);
		for (int j = 0; j < w; j++)
			Console.Write(" ");
	}
}
void			Element::Show()const {}
VK_NAMESPACE_END