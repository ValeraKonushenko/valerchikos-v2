#pragma once
#include "AppManager.h"
#include "CtrlManager.h"
#include "vkdef.h"
#include "WNDGeneral.h"
#include "App.h"
#ifndef VK_NAMESPACE
#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }
#endif 

VK_NAMESPACE_BEG

class OS{
private:
	//~~~~~MANAGERS~~~~~//
	AppManager	app_manager;
	CtrlManager ctrl_manager;

	//~~~~~GENERAL~~~~~//
	using Time = std::chrono::time_point<std::chrono::steady_clock>;
	Time		time;
	PCMouse		mouse;
	Vector2<BYTE> pressed_keys;
	bool		os_is_work;
	EVENTS		event;


	void ProcessInputing();
	void ScreenUpdate();
	void AppUpdate(const Time &time);
	void ShowTaskBar();
	void DisplayTime(const Time &time);
	void SystemUpdate();
public:
	static void * GetPointerToApp(EnumProgramms type);
	OS();
	void Start();
	~OS();
	DISABLE_COPY_CLASS(OS)
};
VK_NAMESPACE_END