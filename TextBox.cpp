#include "vkdef.h"
#include "TextBox.h"


VK_NAMESPACE_BEG
void			TextBox::SetData	(const char * str){
	data = str;
}
void			TextBox::AddData	(const char * str){
	data.Cat(str);
}
const char *	TextBox::GetData	() const {
	return data.c_str();
}
bool			TextBox::IsEmpty	() const {
	return data.IsEmpty();
}
void			TextBox::Show		() const{
	int txt_len = data.Length();
	//INTERPITATOR:
	// \t - 3 space
	// \n - new row
	Color old_color = static_cast<Color>(Console.GetColor());
	int w_row = end.x - beg.x;
	int shifting = 0;

	Console.SetColor(bg, fg);

	//cc - column counter
	//cr - current row
	for (int i = 0, cc = 0, cr = 0; i < txt_len; i++){
		if (beg.x + cc + shifting == end.x && beg.y + cr - cur_scroll == end.y - 1)
			break;
		if (data[i] == '\n' || cc + shifting * 2 == w_row) {
			cr++;
			if (data[i] == '\n') i++;
			cc = 0;
		}
		if (cc == 0 && data[i] == ' ') continue;
		if (data[i] == '\t') { cc += 3; continue; }
		if (cc == 0 && align == Center) {
			shifting = 0;
			short len = 0;
			for (short u = i; u < w_row + i && data[u] && data[u] != '\n' && len < w_row; u++){
				if (u == i && data[u] == ' ')	continue;
				if (data[u] == '\t')
					len += 2;//the next to this 'if' stating the len++, and 2 + 1 = 3
				len++;
			}
			for (short u = len; u > 0 && data[u + i] == ' '; u--)
				len--;
			shifting = (w_row - len) != 0?(w_row - len) / 2: 0;
		}
		else if (cc == 0 && align == ToLeft) {
			//BY DEF
		}
		else if (cc == 0 && align == ToRight) {
			shifting = 0;
			short len = 0;
			for (short u = i; u < w_row + i && data[u] && data[u] != '\n' && len < w_row; u++) {
				if (u == i && data[u] == ' ')	continue;
				if (data[u] == '\t')
					len += 2;//the next to this 'if' stating the len++, and 2 + 1 = 3
				len++;
			}
			for (short u = len; u > 0 && data[u + i] == ' '; u--)
				len--;
			shifting = w_row - len;
		}

		if (cur_scroll <= cr) {
			Console.SetCursorPositon(beg.x + cc + shifting, beg.y + cr - cur_scroll);
			Console.Write(data[i]);
		}
		cc++;
	}
	Console.SetColor(old_color);


	Console.ShowCaret(0);
}
void			TextBox::SetAlign(Align align){
	this->align = align;
}
bool			TextBox::ScrollToDown(short step){
	cur_scroll += step;
	return true;
}
bool			TextBox::ScrollToUp(short step){
	if (cur_scroll > 0) {
		cur_scroll -= step;
		return true;
	}
	return false;
}
Align			TextBox::GetAlign(){
	return Align(align);
}
VK_NAMESPACE_END