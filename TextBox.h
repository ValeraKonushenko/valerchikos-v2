#pragma once
#include "String.h"
#include "Vector2.h"
#include "Element.h"

//TextBox's size
//Align to center
//Align to right/left
//Vertical/horizontal scroll
#ifndef VK_NAMESPACE
#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }
#endif 

VK_NAMESPACE_BEG
class TextBox : public Element {
protected:
	String data;
	short cur_scroll;
	Align align;
public:
	/*INTERFACE*/
	void			SetData(const char *str);
	void			AddData(const char *str);
	const char *	GetData()	const;
	bool			IsEmpty()	const;
	void			Show()		const override;
	void			SetAlign(Align align);
	bool			ScrollToDown(short step = 1);
	bool			ScrollToUp(short step = 1);
	Align			GetAlign();

	/*GENERAL*/
	TextBox() {
		cur_scroll = 0;
		align = Center;
	}
	~TextBox() = default;
	TextBox(const TextBox& n) { *this = n; }
	TextBox(const TextBox&&n) { *this = n; }
	TextBox& operator=(const TextBox& n) {
		data = n.data; 
		beg = n.beg;
		end = n.end;
		cur_scroll = n.cur_scroll;
	}
	TextBox& operator=(const TextBox&& n) { 
		data = n.data;
		beg = n.beg;
		end = n.end;
		cur_scroll = n.cur_scroll;
	}
};
VK_NAMESPACE_END