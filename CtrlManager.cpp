#include "vkdef.h"
#include "CtrlManager.h"
#include "Console.h"
#include <conio.h>
VK_NAMESPACE_BEG
CtrlManager::CtrlManager(): controll(&CtrlManager::Update, this) {
}
void CtrlManager::Update(){
	int lkey, lsub_key;

	while (true) {
		//KEYBOARD
		lkey		= _getch();
		lsub_key	= 0;
		if (lkey == k_IsExpandKeyboar)
			lsub_key = _getch();

		read_data.lock();
		key = lkey;
		sub_key = lsub_key;
		read_data.unlock();
	}
}

CtrlManager::~CtrlManager(){
	controll.detach();
}
VK_NAMESPACE_END
