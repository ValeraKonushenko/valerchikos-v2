#include "vkdef.h"
#include "Grid.h"
#include "Console.h"
#include <cstring>


VK_NAMESPACE_BEG
Grid::Grid(CPoint L_U_corner, CPoint R_D_corner, short row, short col) :
	titles(0.f), beg(L_U_corner), end(R_D_corner) {
	if (row <= 0)throw Exception("Too small amount of rows");
	if (col <= 0)throw Exception("Too small amount of columns");

	this->row = row;
	this->col = col;
	sz_workspace = end - beg;
	titles.Reserve(row * col);
	GridTitle fake;
	for (int i = 0; i < row * col; i++)
		titles.PushBack(fake);

	CPoint step(sz_workspace.x / col, sz_workspace.y / row);
	for (short i = 0; i < row; i++)
		for (short j = 0; j < col; j++)
			GetTitle(j, i).SetPosition(
				{L_U_corner.x + j * step.x,L_U_corner.y + i * step.y}, 
				{ L_U_corner.x + j * step.x + step.x,L_U_corner.y +  i * step.y + step.y }
			);
}
int Grid::GetAmoTitle()const {
	return this->titles.Size();
}

int Grid::GetAmoRow() const {
	return this->row;
}
int Grid::GetAmoCol() const {
	return this->col;
}
void Grid::RebuildAccordintToNewRect(CPoint beg, CPoint end){
	this->beg = beg;
	this->end = end;
	sz_workspace = end - beg;
	CPoint step(sz_workspace.x / col, sz_workspace.y / row);
	for (short i = 0; i < row; i++)
		for (short j = 0; j < col; j++)
			GetTitle(j, i).SetPosition(
				{ this->beg.x + j * step.x,this->beg.y + i * step.y },
				{ this->beg.x + j * step.x + step.x,this->beg.y + i * step.y + step.y }
	);
}



//~~~~~~~~~~~~~~~~TITLE~~~~~~~~~~~~~~~~//
void GridTitle::SetPosition(CPoint beg, CPoint end){
	this->beg = beg;
	this->end = end;
}
GridTitle::GridTitle(){
	data = nullptr;
}
GridTitle& GridTitle::SetData(const char * data){
	if (data == nullptr)throw Exception("You gave a null string");
	int len{}, value = (end.x - beg.x) * (end.y - beg.y);
	len = static_cast<int>(strlen(data));
	if (len > value) len = value;

	//REALLOCATING
	if (this->data)delete[] this->data;
	this->data = new char[len + 1]{ 0 };//+1 - for an '\0'
	for (int i = 0; i < len; i++)
		this->data[i] = data[i];

	/*
	int len{}, value = (end.x - beg.x) * (end.y - beg.y);
	if (_data)len = static_cast<int>(strlen(_data));
	
	if (this->data && *this->data != 0)
		delete[]this->data;
	this->data = new char[value] { 0 };
	for (int i = 0; i < value && i < len; i++)		this->data[i] = _data[i];
	*/
	return *this;
}
const char * GridTitle::GetData(){
	return this->data;
}

void GridTitle::ShowData(bool is_active, bool is_text, bool is_align) const{
	if (this->data == nullptr)return;
	//SIZE OF THE CURRENT TITLE
	short row = end.y - beg.y;
	short col = end.x - beg.x;


	//INVERTING A COLOR
	static unsigned char inv_color = 0;
	if (is_active) {
		inv_color = ~(Console.GetColor());
		Console.SetColor(static_cast<_Console::ConsoleColor>(inv_color));
	}


	//FILL IN THE SPACES OUR TITLE(Clear old data)
	for (int i = 0; i < row; i++) {
		Console.SetCursorPositon(beg.x, beg.y + i);
		for (int j = 0; j < col; j++)
			Console.Write(" ");
	}


	//OUTPUT THE TEXT
	int txt_len = static_cast<int>(strlen(data));
	if (is_text) {
		Console.SetCursorPositon(beg.x, beg.y);//moving to the title's start
		char **txt_row = new char*[row];
		for (int i = 0; i < row; i++)txt_row[i] = new char[col + 1]{ 0 };

		int curr_row = 0, last_curr_row = -1;
		int col_counter = 0;
		int line_feed = -1;
		for (int i = 0; i < row * col && i < txt_len && curr_row < row; i++) {
			if (col_counter == 0 && data[i] == ' ')continue;//Removing ' ' from the begin
			if (last_curr_row != curr_row) {
				//FINDING THE NEXT LINE FEED
				bool is_find_space = false;
				line_feed = i + col;
				last_curr_row = curr_row;
				if (line_feed > txt_len)
					line_feed = txt_len;
				while (line_feed >= 0) {
					if (line_feed >= 0 && (data[line_feed] == ' ' || data[line_feed] == 0)) {
						is_find_space = true;
						break;
					}
					line_feed--;
				}
				if (!is_find_space) {
					line_feed = i + col;
				}
			}
			txt_row[curr_row][col_counter] = data[i];
			col_counter++;
			if (col_counter == col || line_feed == i) {
				curr_row++;
				col_counter = 0;
			}
			}
		//Removing ' ' from the end
		for (int i = 0; i < row && txt_row[i] != 0; i++)
			for (int j = static_cast<int>(strlen(txt_row[i])) - 1; j >= 0; j--)
				if (txt_row[i][j] == ' ')
					txt_row[i][j] = 0;
				else
					break;
		

		static int shifting;
		for (int i = 0; i < row && txt_row[i] != 0; i++) {
			if (is_align)
				shifting = static_cast<int>((col - strlen(txt_row[i])) / 2);
			else
				shifting = 0;
			Console.SetCursorPositon(beg.x + shifting, beg.y + i).Write(txt_row[i]);
		}


		for (int i = 0; i < row; i++)delete[] txt_row[i];
		delete[] txt_row;
	}
	else {
		int col_counter = 0, shifting = 0;
		Console.SetCursorPositon(beg.x, beg.y);
		for (int i = 0; i < txt_len; i++){
			col_counter++;
			Console.Write(data[i]);
			if (col_counter == col) {
				Console.SetCursorPositon(beg.x, beg.y + ++shifting);
				col_counter = 0;
			}
		}
	}

	//INVERTING A COLOR to old version
	if (is_active) {
		inv_color = ~inv_color;
		Console.SetColor(static_cast<_Console::ConsoleColor>(inv_color));
	}
	/*
	static char amo_dots_in_the_end = 2;
	short row = end.y - beg.y;
	short col = end.x - beg.x;
	int shifting = 0, col_counter = 0;
	static unsigned char inv_color = 0;
	if (is_active) {
		inv_color = ~(Console.GetColor());
		Console.SetColor(static_cast<_Console::ConsoleColor>(inv_color));
	}
	for (int i = 0; i < row; i++){
		Console.SetCursorPositon(beg.x, beg.y + i);
		for (int j = 0; j < col; j++)
			Console.Write(" ");
	}

	Console.SetCursorPositon(beg.x, beg.y + shifting);
	for (int i = 0; i < row*col && data[i] && shifting < row; i++) {
		Console.Write(data[i]);
		col_counter++;
		if (GetLenToSpace(data + i) == 0 || col_counter >= col) {

			Console.SetCursorPositon(beg.x, beg.y + ++shifting);
			if(col_counter < col)
				for (; i < row*col && data[i] && data[i] != ' '; i++);
			col_counter = 0;
		}
		if(row*col - i > amo_dots_in_the_end && shifting == row - 1 
			&& col - col_counter == amo_dots_in_the_end){
			for (; col_counter < col; col_counter++)
					Console.Write(".");
			break;
		}
	}
	if (is_active) {
		inv_color = ~inv_color;
		Console.SetColor(static_cast<_Console::ConsoleColor>(inv_color));
	}*/
}
CPoint GridTitle::GetPBeg(){
	return beg;
}
CPoint GridTitle::GetPEnd(){
	return end;
}
short GridTitle::GetW(){
	return end.x - beg.x;
}
short GridTitle::GetH() {
	return end.y - beg.y;
}
GridTitle::~GridTitle() {
	if (data)delete[]data;
}

GridTitle & Grid::GetTitle(short x, short y){
	//if (x >= col || y >= row || y < 0 || x < 0)throw Exception("Read access violation");
	while (x >= col)	x -= col;
	while (x < 0)		x += col;
	while (y >= row)	y -= row;
	while (y < 0)		y += row;
	return titles[y * col + x];
}
GridTitle & Grid::GetTitle(short i){	
	while (i < 0)i += col * row;
	return this->titles[i];
}
VK_NAMESPACE_END
