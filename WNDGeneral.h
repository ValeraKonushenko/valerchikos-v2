#pragma once
#include "vkdef.h"
#include "Console.h"

#ifndef VK_NAMESPACE
#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }
#endif 

VK_NAMESPACE_BEG
#define WINDOW_EDITOR___RESERV_K_HOME(wnd) \
			if (sub_key == k_Home_e) \
			return { VOS_START_APP, \
			new AppEditWnd(&wnd,wnd.GetPos().x + 1,wnd.GetPos().y + 1) };

using Color = _Console::ConsoleColor;
enum class EnumProgramms: short {
	none,
	AppFileManager,
	AppMiner,
	AppTelegramBot,
	AppSettings,
	AppHelper
};
enum WNDRules {
	border_w	= 1,
	min_wnd_h	= 4,
	min_wnd_w	= 10,
	grid_sz_w	= 4,
	grid_sz_h	= 2,
	freq_ms		= 500
};
enum BorderType {
	bt_none, bt_single, bt_double
};
VK_NAMESPACE_END