#include "vkdef.h"
#include "Explorer.h"
#include "WNDGeneral.h"
#include "String.h"
#include "OS.h"
#include "AppEditWnd.h"
#include <ctime>
#pragma warning (disable: 4996)//for a locale
VK_NAMESPACE_BEG
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~EXPLORER~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void Explorer::GetNextTitleInGrid(Grid &grid, Pair<short, 
	const GridTitle*>&title){
	int amo = grid.GetAmoTitle();

	//Finding next title between position of x and amo(i.g: 78 - 120)
	if (title.x >= 0 && title.x < amo)
		for (int i = title.x + 1; i < amo; i++)
			if (grid.GetTitle(i).GetData() && grid.GetTitle(i).GetData()[0] != 0) {
				//if element is found
				title.x = i;
				title.y = &grid.GetTitle(i);
				return;
			}

	//Finding next title between position of 0 and amo(i.g: 0 - 120)
	for (int i = 0; i < amo; i++)
		if (grid.GetTitle(i).GetData() && grid.GetTitle(i).GetData()[0] != 0) {
			//if element is found
			title.x = i;
			title.y = &grid.GetTitle(i);
			return;
		}

	return;
}
void Explorer::GetNextTitleInGridByDir(Grid&grid, 
	Pair<short, const GridTitle*> &choosed_title, BYTE Dir){
	int amo = grid.GetAmoTitle();
	if (choosed_title.x < 0 || choosed_title.x >= amo || !choosed_title.y) {
		GetNextTitleInGrid(grid, choosed_title);
		return;
	}
	int row, col, search_from_i;
	row = grid.GetAmoRow();
	col = grid.GetAmoCol();

	if (Dir == Direction::dir_Down) {
		search_from_i = (choosed_title.x / col) * col + col;
		for (int i = search_from_i; i < row*col; i++)
			if (grid.GetTitle(i).GetData() && grid.GetTitle(i).GetData()[0] != 0) {
				choosed_title.x = i;
				choosed_title.y = &grid.GetTitle(i);
				break;
			}
	}
	else if (Dir == Direction::dir_Up) {
		search_from_i = (choosed_title.x / col) * col - 1;
		for (int i = search_from_i; i >= 0; i--)
			if (grid.GetTitle(i).GetData() && grid.GetTitle(i).GetData()[0] != 0) {
				choosed_title.x = i;
				choosed_title.y = &grid.GetTitle(i);
				break;
			}
	}
	else if (Dir == Direction::dir_Left) {
		int curr_col = choosed_title.x % col - 1;
		for (int i = choosed_title.x - 1; i >= 0 && curr_col >= 0; i--,curr_col--)
			if (grid.GetTitle(i).GetData() && grid.GetTitle(i).GetData()[0] != 0) {
				choosed_title.x = i;
				choosed_title.y = &grid.GetTitle(i);
				break;
			}

	}
	else {
		int curr_col = choosed_title.x % col + 1;
		for (int i = choosed_title.x + 1; i < amo && curr_col < col; i++, curr_col++)
			if (grid.GetTitle(i).GetData() && grid.GetTitle(i).GetData()[0] != 0) {
				choosed_title.x = i;
				choosed_title.y = &grid.GetTitle(i);
				break;
			}
	}

}
void Explorer::SetAppShortcut(short grid_x, short grid_y, 
	EnumProgramms id, const char * title){
	int col = desktop.grid.GetAmoCol();
	int row = desktop.grid.GetAmoRow();
	while (grid_x >= col)	grid_x -= col;
	while (grid_x < 0)		grid_x += col;
	while (grid_y >= row)	grid_y -= row;
	while (grid_y < 0)		grid_y += row;
	desktop.grid.GetTitle(grid_x, grid_y).SetData(title);
	ref_to_prog[grid_y * desktop.grid.GetAmoCol() + grid_x] = id;
}
EnumProgramms Explorer::GetAppShortcut(short grid_x, short grid_y)const{
	int col = desktop.grid.GetAmoCol(), row = desktop.grid.GetAmoRow();
	while (grid_x >= col)	grid_x -= col;
	while (grid_x < 0)		grid_x += col;
	while (grid_y >= row)	grid_y -= row;
	while (grid_y < 0)		grid_y += row;
	return ref_to_prog[grid_y * desktop.grid.GetAmoCol() + grid_x];
}
EnumProgramms Explorer::GetAppShortcut(Vector2<short> p)const {
	int col = desktop.grid.GetAmoCol(), row = desktop.grid.GetAmoRow();
	while (p.x >= col)	p.x -= col;
	while (p.x < 0)		p.x += col;
	while (p.y >= row)	p.y -= row;
	while (p.y < 0)		p.y += row;
	return ref_to_prog[p.y * desktop.grid.GetAmoCol() + p.x];
}
EnumProgramms Explorer::GetAppShortcut(short p)const {
	if (p >= desktop.grid.GetAmoTitle() || p < 0)throw Exception("The value is incorrect");
	return ref_to_prog[p];
}
Explorer::Explorer(){

	mwnd = &desktop;
	int amo_refs = desktop.grid.GetAmoTitle();
	ref_to_prog = new EnumProgramms[amo_refs];
	for (int i = 0; i < amo_refs; i++)
		ref_to_prog[i] = EnumProgramms::none;
	SetAppShortcut(0, 0, EnumProgramms::AppHelper,		"OS Helper");
	SetAppShortcut(1, 0, EnumProgramms::AppSettings,	"OS Settings");
	SetAppShortcut(-1, 3, EnumProgramms::AppFileManager, "File Manger");
	SetAppShortcut(-1, 4, EnumProgramms::AppMiner,		"Miner Game!");
	SetAppShortcut(-1, 5, EnumProgramms::AppTelegramBot, "Telegram Bot");
}
Explorer::~Explorer(){
	delete[] this->ref_to_prog;
}
Pair<EVENTS, void*> Explorer::OnKeyDown(unsigned char key, 
	unsigned  char sub_key) {

	if (key == k_IsExpandKeyboar) {
		if (sub_key == k_Down_e || sub_key == k_Up_e || sub_key == k_Left_e || sub_key == k_Right_e) {
			_UPDATE_PART_OF_THE_SCREEN__CLEAR
				if (desktop.choosed_title.y)
					desktop.choosed_title.y->ShowData(0, 1, 1);
			if (sub_key == k_Down_e)
				GetNextTitleInGridByDir(desktop.grid, desktop.choosed_title, dir_Down);
			else if (sub_key == k_Up_e)
				GetNextTitleInGridByDir(desktop.grid, desktop.choosed_title, dir_Up);
			else if (sub_key == k_Left_e)
				GetNextTitleInGridByDir(desktop.grid, desktop.choosed_title, dir_Left);
			else
				GetNextTitleInGridByDir(desktop.grid, desktop.choosed_title, dir_Right);

			_UPDATE_PART_OF_THE_SCREEN__DISPLAY
				if (desktop.choosed_title.y)
					desktop.choosed_title.y->ShowData(1, 1, 1);
			return { VOS_KEY_WAS_USED, nullptr };
		}
		//WINDOW_EDITOR___RESERV_K_HOME(this->desktop)
	}
	else if (key == k_Enter) {
		App* out = reinterpret_cast<App*>(
			OS::GetPointerToApp(GetAppShortcut(desktop.choosed_title.x)));
		if (out != nullptr)
			return { VOS_START_APP, out };
		return { VOS_KEY_WAS_USED, nullptr };
	}
	return {VOS_KEY_NOT_USED, nullptr };
}
Pair<EVENTS, void*> Explorer::OnMouse(const PCMouse &mouse){
	//TARGETING ON SOME TITLE
	int amo_title = desktop.grid.GetAmoTitle();
	static short last_active_title = -1;
	static short last_hide_title = -1;

	for (int i = 0; i < amo_title; i++)
		if (mouse.pos.X >= desktop.grid.GetTitle(i).GetPBeg().x &&
			mouse.pos.X < desktop.grid.GetTitle(i).GetPBeg().x + desktop.grid.GetTitle(i).GetW() &&
			mouse.pos.Y >= desktop.grid.GetTitle(i).GetPBeg().y &&
			mouse.pos.Y < desktop.grid.GetTitle(i).GetPBeg().y + desktop.grid.GetTitle(i).GetH() &&
			desktop.grid.GetTitle(i).GetData() != nullptr && desktop.grid.GetTitle(i).GetData()[0])
		{
			if (last_active_title != i) {
				desktop.grid.GetTitle(i).ShowData(1, 1, 1);
				desktop.grid.GetTitle(last_active_title).ShowData(0, 1, 1);
				this->desktop.choosed_title.x = i;
				this->desktop.choosed_title.y = &desktop.grid.GetTitle(last_active_title);
				last_active_title = i;
			}
			if (mouse.is_left) {
				this->desktop.choosed_title.x = i;
				this->desktop.choosed_title.y = &desktop.grid.GetTitle(last_active_title);
				App* out = reinterpret_cast<App*>(
					OS::GetPointerToApp(GetAppShortcut(desktop.choosed_title.x)));
				if (out != nullptr)
					return { VOS_START_APP, out };
			}
			return Pair<EVENTS, void*>(VOS_KEY_NOT_USED, nullptr);
		}
	return Pair<EVENTS, void*>(0,nullptr);
}
void Explorer::OnDraw(){
	desktop.Show();
}
void Explorer::Update(const Time &time) {
	//desktop.BlinkActiveTitle(time);
	Console.SetColor(this->desktop.bg, this->desktop.fg);
	static short last_wnd_width = desktop.w;
	static short last_wnd_height = desktop.h;
	if (desktop.IsFullScreen() == FullScreen) {
		if (Console.GetWndCols() != last_wnd_width || Console.GetWndRows() != last_wnd_height) {
			last_wnd_width = Console.GetWndCols();
			last_wnd_height = Console.GetWndRows();
			desktop.grid.RebuildAccordintToNewRect({ border_w,border_w },
				{ last_wnd_width - border_w, last_wnd_height - border_w });

		}
	}
}


/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~DESKTOP~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

void Explorer::Desktop::BlinkActiveTitle(const Time & time){
	static Time		last_blink;
	static bool		is_blink = false;
	if (!this->choosed_title.y)return;
	if (std::chrono::duration_cast<std::chrono::milliseconds>(time - last_blink).count() >= 500) {
		if (!is_blink) {
			this->choosed_title.y->ShowData(1);
			is_blink = true;
		}
		else {
			this->choosed_title.y->ShowData(0);
			is_blink = false;
		}
		last_blink = time;
	}
}
Explorer::Desktop::Desktop():
	Window("DESKTOP",0,0,Console.GetWndCols(), Console.GetWndRows(),
		Color::Gray,Color::White,BorderType::bt_double, FullScreen),
	grid({ border_w ,border_w }, 
		{ Console.GetWndCols() - border_w, Console.GetWndRows() - border_w}
		,8,12) 
	{
	choosed_title.x = -1;
	choosed_title.y = nullptr;

	
}
void Explorer::Desktop::Draw(){
	Console.ShowCaret(0);
	static int amo;
	amo = grid.GetAmoTitle();
	for (int i = 0; i < amo; i++)
		if (grid.GetTitle(i).GetData() && grid.GetTitle(i).GetData()[0] != 0)
			grid.GetTitle(i).ShowData(0,1,1);
		
	//DISPLAY ACTIVE TITLE//
	if(choosed_title.y)
		choosed_title.y->ShowData(1,1,1);
}
VK_NAMESPACE_END
#pragma warning (default: 4996)