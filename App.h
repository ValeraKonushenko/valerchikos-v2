#pragma once
#include "vkdef.h"
#include "Vector2.h"
#include "Pair.h"
#include "Window.h"
#include <chrono>
#include <Windows.h>

#ifndef VK_NAMESPACE
#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }
#endif 

VK_NAMESPACE_BEG
struct PCMouse {
	bool is_left;
	bool is_right;
	COORD pos;
};
class App {
protected:
	Window *mwnd;
public:
	virtual Pair<EVENTS, void*> OnKeyDown(unsigned char key, unsigned  char sub_key) {
		key = sub_key = 0;
		return { 0,nullptr };
	}
	using Time = std::chrono::time_point<std::chrono::steady_clock>;
	virtual void Update(const Time &time) {};
	virtual void OnDraw(){}
	virtual Pair<EVENTS, void*> OnMouse(const PCMouse &mouse){
		return { 0,nullptr };
	}
	App* GetOBJ() {
		return this;
	}
	const Window * GetWnd()const {
		return mwnd;
	}

	App()						= default;
	virtual ~App()				= default;
	DISABLE_COPY_CLASS(App)
};
VK_NAMESPACE_END