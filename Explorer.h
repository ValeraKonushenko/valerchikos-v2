#pragma once
#include "Window.h"
#include "App.h"
#include "Grid.h"
#include "Pair.h"

#ifndef VK_NAMESPACE
#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }
#endif 

VK_NAMESPACE_BEG
struct Shortcut {
	GridTitle *title;
	vk::EnumProgramms id_prog;
};

class Explorer : public App {
private:
	class Desktop : public Window {
	public:
		friend class Explorer;
		Desktop();
		Grid grid;
		void Draw()override;

		Pair<short, const GridTitle*> choosed_title;
		
		void BlinkActiveTitle(const Time &time);
	}desktop;
	vk::EnumProgramms *ref_to_prog;
	void GetNextTitleInGrid(Grid&, Pair<short, const GridTitle*> &);
	void GetNextTitleInGridByDir(Grid&, Pair<short, const GridTitle*> &, BYTE);
public:
	void SetAppShortcut(short grid_x, short grid_y, EnumProgramms id, const char * title);
	EnumProgramms GetAppShortcut(short grid_x, short grid_y)const;
	EnumProgramms GetAppShortcut(Vector2<short> p)const;
	EnumProgramms GetAppShortcut(short p)const;
	Explorer();
	~Explorer();
	Pair<EVENTS, void*> OnKeyDown(unsigned char key, unsigned  char sub_key)override;
	Pair<EVENTS, void*> OnMouse(const PCMouse &mouse)override;
	void Update(const Time &time)override;
	void OnDraw()override;
	DISABLE_COPY_CLASS(Explorer)
};
VK_NAMESPACE_END