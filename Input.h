#pragma once
#include "Element.h"
#include "Console.h"
#include "String.h"
#include "vkdef.h"

#ifndef VK_NAMESPACE
#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }
#endif 

VK_NAMESPACE_BEG
class Input : public Element{
private:
	Color field_bg, field_fg;
	String title;
	String data;
	short max_input_len;
	int w;
	BYTE lock;
	short pos;
	bool is_active;
	
public:
	Input() {
		is_active = false;
		title = "";
		data = "";
		pos = 0;
		w = max_input_len = 0;
		lock = LOCK__NOT;
		field_bg = Color::Blue;
		field_fg = Color::BrightWhite;
		data.Reserve(STR_SZ);
		data.FillZero(STR_SZ);
		end.y = beg.y + 1;
		end.x = beg.x;
	}
	void SetActive(bool act) {
		is_active = act;
	}
	bool GetIsActive()const {
		return is_active;
	}
	short GetMaxInputLen() const {
		return max_input_len;
	}
	void SetMaxInputLen(short len) {
		max_input_len = len;
		end.x = beg.x + len + title.Length();
	}
	void SetPosition(short x, short y) {
		beg = { x, y };
		end.x = beg.x + max_input_len + title.Length();
		end.y = beg.y + 1;
	}
	void SetTitle(const char *str) {
		title = str;
		end.x = beg.x + max_input_len + title.Length();
	}
	void SetData(const char * str) {
		data = str;
		pos = data.Length();
	}
	const char * GetTitle()const {
		return title.c_str();
	}
	const char * GetData()const {
		return data.c_str();
	}
	void SetColorForField(Color bg, Color fg) {
		field_bg = bg;
		field_fg = fg;
	}
	short GetCaret()const {
		return pos;
	}
	BYTE GetLock()const {
		return lock;
	}
	void SetLock(BYTE lock) {
		this->lock = lock;
	}
	void Show() {
		//TITLE
		Console.SetColor(bg, fg);
		Console.SetCursorPositon(beg.x, beg.y).Write(title);

		//FIELD
		WORD old_color = Console.GetColor();
		if(is_active)	
			Console.SetColor(Color::Red, field_fg);
		else			Console.SetColor(field_bg, field_fg);

		for (int i = 0; i < max_input_len; i++)
			Console.Write(" ");
		Console.SetCursorPositon(beg.x + title.Length(), beg.y);
		for (int i = 0; i < max_input_len && data[i] != 0; i++)
			Console.Write(data[i]);

		if (is_active)	Console.SetColor(static_cast<Color>(old_color));
	}
	bool InputData(BYTE key, BYTE sub_key) {
		if (key == 0)return false;
		//EXPAND KEYBOARD
		if (key == k_Backspace) {
			if (pos > 0) {
				data.Erase(--pos,1);
				return true;
			}
			return false;
		}
		else if (key == k_IsExpandKeyboar) {
			if (sub_key == k_Left_e) {
				if (pos > 0)pos--;
			}
			else if (sub_key == k_Right_e) {
				if (pos < data.Length())pos++;
			}
		}

		//0000'0000
		//0000'1001

		//LOCK
		if (pos >= max_input_len && data.Length() <= max_input_len)return false;
		if ((lock | LOCK__NOT) == LOCK__NOT) {
			data.Insert(pos++, key);
			return true;
		}
		if ((lock & LOCK__ALPHA) == LOCK__ALPHA) {
			//     65 - 'A'    90 - 'Z'    97 - 'a'    122 - 'z'
			if (key >= 65 && key <= 90 || key >= 97 && key <= 122) {
				data.Insert(pos++, key);
				return true;
			}
			return false;
		}
		if ((lock & LOCK__DIGIT) == LOCK__DIGIT) {
			if (key >= 48 && key <= 57) {
				data.Insert(pos++, key);
				return true;
			}
			return false;
		}
		if ((lock & LOCK__EXTRA) == LOCK__EXTRA) {
			if (key >= 32 && key <= 47 || key >= 58 && key <= 64 ||
				key >= 91 && key <= 96) {
				data.Insert(pos++, key);
				return true;
			}
			return false;
		}
		return false;
	}
	
};
VK_NAMESPACE_END