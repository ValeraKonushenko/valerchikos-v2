#include "vkdef.h"
#include "OS.h"
#include "Input.h"

//~~~~~~~~~~APPLICATIONS FOT A WORK~~~~~~~~~~//
#include "AppFileManager.h"
#include "Explorer.h"
#include "AppOSHelper.h"

#include <ctime>
#include <chrono>

VK_NAMESPACE_BEG
void * OS::GetPointerToApp(EnumProgramms type) {
	if (type == EnumProgramms::AppFileManager)
		return new AppFileManager;
	else if(type == EnumProgramms::AppHelper)
		return new AppOSHelper;
		

	return nullptr;
}
OS::OS(){
	Console.SetTitle("Valerchik OS");
	app_manager.PushApp(new Explorer);
	os_is_work = true;
	event = VOS_SCREEN_UPDATE;
	pressed_keys = 0;
}
void OS::ScreenUpdate(){
	if ((event & VOS_SCREEN_UPDATE) == VOS_SCREEN_UPDATE) {
		app_manager.UpdateAllWindow();
		ShowTaskBar();
	}
	event = 0;
}
void OS::AppUpdate(const Time &time){
	static short amo_app = app_manager.GetAmoApp();
	amo_app = app_manager.GetAmoApp();
	for (int i = 0; i < amo_app; i++)
		app_manager.GetApp(i)->Update(time);
	//app_manager.GetLastApp()->Update(time);///last version
}
void OS::ShowTaskBar(){
	static int length = Console.GetWndCols();
	length = Console.GetWndCols();
	Console.SetColor(app_manager.GetApp(0)->GetWnd()->GetBgColor(),
		app_manager.GetApp(0)->GetWnd()->GetFgColor());
	for (int i = length - 1; i >= 0; i--)
		Console.SetCursorPositon(i, Console.GetWndRows() - 1)
		.Write(static_cast<char>(177));
	Console.SetCursorPositon(0, 0);
}
void OS::DisplayTime(const Time & _time){
	static Time		last_update;
	static int		last_display = 0;
	static short	one_second = 1000;//ms
	static time_t	seconds = 0;
	static tm*		timeinfo = 0;
	static int		standart_len_timeline = 25;
	static String	display_time(30);

	if (std::chrono::duration_cast<std::chrono::milliseconds>
		(_time - last_update).count() >= one_second) {
		_UPDATE_PART_OF_THE_SCREEN__CLEAR
		Console.SetCursorPositon(Console.GetWndCols() - standart_len_timeline,
			Console.GetWndRows() - 1);
		static int del_len = display_time.Length();
		del_len = display_time.Length();
		for (int i = 0; i < del_len; i++)
			Console.Write(static_cast<char>(177));


		_UPDATE_PART_OF_THE_SCREEN__DISPLAY
		seconds = ::time(NULL);
		timeinfo = localtime(&seconds);
		Console.SetCursorPositon(Console.GetWndCols() - standart_len_timeline,
			Console.GetWndRows() - 1);
		display_time = "|";
		display_time += ConvertIntToString(timeinfo->tm_mon);
		display_time += "/";
		display_time += ConvertIntToString(timeinfo->tm_mday);
		display_time += "/";
		display_time += ConvertIntToString(1900 + timeinfo->tm_year);
		display_time += "   ";
		display_time += ConvertIntToString(timeinfo->tm_hour);
		display_time += ":";
		display_time += ConvertIntToString(timeinfo->tm_min);
		display_time += ":";
		display_time += ConvertIntToString(timeinfo->tm_sec);
		display_time += "|";

		Console.Write(static_cast<const char*>(display_time));


		last_update = _time;
	}
}
void OS::SystemUpdate(){
	static short last_wnd_w = Console.GetWndCols();
	static short last_wnd_h = Console.GetWndRows();
	if (Console.GetWndCols() != last_wnd_w || Console.GetWndRows() != last_wnd_h) {
		
		last_wnd_w = Console.GetWndCols();
		last_wnd_h = Console.GetWndRows();
		event = VOS_SCREEN_UPDATE;
	}

	time = std::chrono::high_resolution_clock::now();
	DisplayTime(time);
}
void OS::ProcessInputing() {
	//GETING A KEY//
	pressed_keys = ctrl_manager.GetKey();
	mouse.is_left = Console.GetMouseLButton();
	
	//mouse click delay
	static Time last_mouse;
	if (mouse.is_left && std::chrono::duration_cast<std::chrono::milliseconds>
		(time - last_mouse).count() > 150) {
		last_mouse = time;
	}
	else
		mouse.is_left = false;

	mouse.is_right = Console.GetMouseRButton();
	mouse.pos = Console.GetMouseOnFontPosition();


	/*DEBUG*/
	//if (pressed_keys.x == k_IsExpandKeyboar) {
	//	int y = 2;
	//}
	//if (pressed_keys.y == k_Down_e) {
	//	int y = 2;
	//}
	static Pair<EVENTS, void*> tmp_in;
	static Pair<EVENTS, void*> in;

	//KEYBOARD
	in = app_manager.GetLastApp()->
		OnKeyDown(pressed_keys.x, pressed_keys.y);
	event |= in.x;

	//MOUSE
	tmp_in = app_manager.GetLastApp()->OnMouse(mouse);


	if (tmp_in.x != VOS_FALSE)
		event |= tmp_in.x;
	if (tmp_in.y != nullptr)
		in.y = tmp_in.y;


	//IF THE APP NOT PROCESSED THE INPUTING, then:
	if ((event | VOS_KEY_NOT_USED) == VOS_KEY_NOT_USED) {
		if (pressed_keys.x == k_IsExpandKeyboar)
		{
			if (pressed_keys.y == k_F12_e)
				this->os_is_work = false;
		}
		if (pressed_keys.x == k_ESC && app_manager.GetAmoApp() > 1) {
			this->app_manager.PopApp();
			event = VOS_SCREEN_UPDATE;
		}
		if (mouse.is_left && app_manager.GetAmoApp() > 1) {
			const Window *w = app_manager.GetLastApp()->GetWnd();
			//if (w == nullptr)
			//	throw Exception("Pointer to the windows is NULL. Initialize your window: 'mwnd'");
			if ((mouse.pos.X < w->GetPos().x || mouse.pos.X >= w->GetPos().x + w->GetW() ||
				mouse.pos.Y < w->GetPos().y || mouse.pos.Y >= w->GetPos().y + w->GetH()) &&
				(mouse.pos.X >= 0 && mouse.pos.X < Console.GetWndCols() && 
				 mouse.pos.Y >= 0 && mouse.pos.Y < Console.GetWndRows())) {
				this->app_manager.PopApp();
				event = VOS_SCREEN_UPDATE;
			}
		}
	}
	if ((event & VOS_START_APP) == VOS_START_APP && in.y != nullptr) {
		app_manager.PushApp(reinterpret_cast<App*>(in.y));
		reinterpret_cast<App*>(in.y)->OnDraw();
	}
	if ((event & VOS_CLOSE_LAST_APP) == VOS_CLOSE_LAST_APP) {
		app_manager.PopApp();
		event |= VOS_SCREEN_UPDATE;
	}

	//CLEARING A RECIEVED KEY//
	pressed_keys	= 0;
	mouse.is_left	= 0;
	mouse.is_right	= 0;
	mouse.pos		= { 0, 0 };
}
void OS::Start() {
	while (os_is_work) {
		ScreenUpdate();
		AppUpdate(time);
		ProcessInputing();
		SystemUpdate();
	}
}
OS::~OS() {
}
VK_NAMESPACE_END