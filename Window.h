#pragma once
#include "vkdef.h"
#include "Stack.h"
#include "Console.h"
#include "String.h"
#include "WNDGeneral.h"
#include "vkdef.h"
#include <Windows.h>
#ifndef VK_NAMESPACE
#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }
#endif 

VK_NAMESPACE_BEG
/********************************************************************************/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~WINDOWS~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/********************************************************************************/
class Window{
protected:
	SHORT				x, y;
	SHORT				w_x, w_y;//work space
	SHORT				wm_x, wm_y;//max work space
	uint				w, h;
	String				title;
	Color				bg, fg;
	const BorderType	bt_type;
	ShowMode			show_mode;
	void LocalSetCursorPosition(SHORT x, SHORT y);
	virtual void		Draw() {}
public:
	void			Show();
	bool			IsFullScreen();
	Window(const char* title, SHORT x, SHORT y, uint w, uint h, Color bg,
		Color fg = Color::BrightWhite, BorderType bt_type = bt_single, ShowMode show_mode = InWindow);
	CPoint			GetPos()const;
	SHORT			GetW()const;
	SHORT			GetH()const;
	ShowMode		GetShowMode()const;
	void			SetPosition(SHORT x, SHORT y);
	void			SetSize(uint w, uint h);
	Color			GetBgColor()const { return bg; }
	Color			GetFgColor()const { return fg; }
	void			MakeShowModeAs_FULL_SCREEN();
	void			MakeShowModeAs_IN_WINDOW(SHORT x, SHORT y, uint w, uint h);
	Window(Window&)				= default;
	Window& operator=(Window&)	= default;
	~Window()					= default;
};
VK_NAMESPACE_END