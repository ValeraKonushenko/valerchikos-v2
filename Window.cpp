#include "vkdef.h"
#include "Window.h"



VK_NAMESPACE_BEG
Window::Window(const char*  title, SHORT x, SHORT y, uint w, uint h, Color bg, Color fg, BorderType bt_type,
	ShowMode show_mode) :
	x(x), y(y), w(w), h(h), title(title), bg(bg), fg(fg), bt_type(bt_type), show_mode(show_mode){
	w_x = x + border_w;
	w_y = y + border_w;
	wm_x = w_x + w - 1 - border_w;
	wm_y = w_y + h - 1 - border_w;
}
CPoint			Window::GetPos()const {
	return CPoint(x, y);
}
SHORT			Window::GetW()const {
	return w;
}
SHORT			Window::GetH()const {
	return h;
}
ShowMode		Window::GetShowMode()const {
	return show_mode;
}
void			Window::SetPosition(SHORT x, SHORT y) {
	this->x = x;
	this->y = y;
	w_x = x + border_w;
	w_y = y + border_w;
	wm_x = w_x + w - 1 - border_w;
	wm_y = w_y + h - 1 - border_w;
}
void			Window::SetSize(uint w, uint h) {
	this->w = w;
	this->h = h;
	wm_x = w_x + w - 1 - border_w;
	wm_y = w_y + h - 1 - border_w;
}
void			Window::Show() {
	Console.SetColor(bg, fg);
	static short last_wnd_width = w;
	static short last_wnd_height = h;
	if (show_mode == FullScreen) {
		if (Console.GetWndCols() != last_wnd_width || Console.GetWndRows() != last_wnd_height) {
			w = last_wnd_width = Console.GetWndCols();
			h = last_wnd_height = Console.GetWndRows();
			w_x = x + border_w;
			w_y = x + border_w;
			wm_x = w_x + w - 1 - border_w;
			wm_y = w_y + h - 1 - border_w;
		}
	}

	uint i = 0, j = 0;

	if (bt_type == bt_single) {
		//CORNERS
		Console.SetCursorPositon(x, y).Write((char)218);
		Console.SetCursorPositon(x + w - 1, y).Write((char)191);
		Console.SetCursorPositon(x, y + h - 1).Write((char)192);
		Console.SetCursorPositon(x + w - 1, y + h - 1).Write((char)217);
		uint title_len = title.Length();

		//HORIZONTAL
		Console.SetCursorPositon(x + title_len + 1, y);
		for (i = x + title_len + 1; i < x + w - border_w; i++)
			Console.Write((char)196);

		Console.SetCursorPositon(x + border_w, y + h - 1);
		for (i = x + border_w; i < x + w - border_w; i++)
			Console.Write((char)196);


		//VERTICAL
		for (i = y + border_w; i < y + h - border_w; i++) {
			Console.SetCursorPositon(x, i);
			Console.Write((char)179);
			Console.SetCursorPositon(x + w - 1, i);
			Console.Write((char)179);
		}
	}
	else if (bt_type == bt_double) {
		//CORNERS
		Console.SetCursorPositon(x + w - 1, y + h - 1).Write((char)188);
		Console.SetCursorPositon(x, y + h - 1).Write((char)200);
		Console.SetCursorPositon(x, y).Write((char)201);
		Console.SetCursorPositon(x + w - 1, y).Write((char)187);
		uint title_len = title.Length();
		if (title_len > w - border_w * 2) {
			title_len = w - border_w * 2;
			title[title_len] = 0;
		}

		//HORIZONTAL
		Console.SetCursorPositon(x + title_len + 1, y);
		for (i = x + title_len + 1; i < x + w - border_w; i++)
			Console.Write((char)205);

		Console.SetCursorPositon(x + border_w, y + h - 1);
		for (i = x + border_w; i < x + w - border_w; i++)
			Console.Write((char)205);


		//VERTICAL
		for (i = y + border_w; i < y + h - border_w; i++) {
			Console.SetCursorPositon(x, i);
			Console.Write((char)186);
			Console.SetCursorPositon(x + w - 1, i);
			Console.Write((char)186);
		}
	}
	else if (bt_type == bt_none) {
		//DO NOTHING
	}


	//TITLE
	Console.SetCursorPositon(x + border_w, y);
	for (int i = 0; i < w - border_w * 2 && title[i] != 0; i++)
		Console.Write(title[i]);


	//MAIN SPACE
	for (i = y + border_w; i < y + h - border_w; i++) {
		Console.SetCursorPositon(x + border_w, i);
		for (j = x + border_w; j < x + w - border_w; j++)
			Console.Write(" ");
	}


	Draw();
}
bool			Window::IsFullScreen(){
	return show_mode;
}
void			Window::LocalSetCursorPosition(SHORT wx, SHORT wy){
	short width = wm_x - w_x;
	short height = wm_y - w_y;
	while (wx >= width)	wx -= width;
	while (wx < 0)		wx += width;
	while (wy >= height)wy -= height;
	while (wy < 0)		wy += height;
	Console.SetCursorPositon(this->w_x + wx, this->w_y + wy);
}
void			Window::MakeShowModeAs_FULL_SCREEN() {
	this->show_mode = FullScreen;
	w = Console.GetWndCols();
	h = Console.GetWndRows();
	x = y = 0;
	w_x = x + border_w;
	w_y = y + border_w;
	wm_x = w_x + w - 1 - border_w;
	wm_y = w_y + h - 1 - border_w;
}
void			Window::MakeShowModeAs_IN_WINDOW(SHORT x, SHORT y, uint w, uint h) {
	this->show_mode = InWindow;
	this->w = w;
	this->h = h;
	this->x = x;
	this->y = y;
	w_x = x + border_w;
	w_y = y + border_w;
	wm_x = w_x + w - 1 - border_w;
	wm_y = w_y + h - 1 - border_w;
}

VK_NAMESPACE_END
