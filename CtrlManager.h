#pragma once
#include "vkdef.h"
#include "Vector2.h"
#include <thread>
#include <mutex>
#include <Windows.h>

#ifndef VK_NAMESPACE
#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }
#endif 

VK_NAMESPACE_BEG
class CtrlManager {
private:
	BYTE key;
	BYTE sub_key;


	void Update();
	std::thread controll;
	std::mutex read_data;
public:
	inline Vector2<BYTE> GetKey	() {
		static Vector2<BYTE> tmp;
		read_data.lock();
		tmp.x = key;tmp.y = sub_key;
		read_data.unlock();
		key = sub_key = 0;
		return tmp;
	}

	CtrlManager();
	~CtrlManager();
	DISABLE_COPY_CLASS(CtrlManager)
};
VK_NAMESPACE_END